<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Contracts\Service\Attribute\Required;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required'=> 'Masukkan Nama!!!',
            'umur.required'=>'Umur harus diisi',
            'bio.required'=>'Biodata harus diisi'
        ]);
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required'=> 'Masukkan Nama!!!',
            'umur.required'=>'Umur harus diisi',
            'bio.required'=>'Biodata harus diisi'
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
              );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
